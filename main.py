import pygame as pg
import random, json, subprocess, os, platform

pg.init()

with open('settings.json') as json_file:
	settings = json.loads(json_file.read())

pause = settings["pauseOnStart"]

def pixel(surface, color, x, y):
	pg.draw.rect(surface, color, (x*settings["cellSize"], y*settings["cellSize"], x*settings["cellSize"]+settings["cellSize"], y*settings["cellSize"]+settings["cellSize"]))

def out(surface, color, x, y):
	pg.draw.rect(surface, color, (x*settings["cellSize"], y*settings["cellSize"], x*settings["cellSize"]+settings["cellSize"], y*settings["cellSize"]+settings["cellSize"]), width=1)

def count_neighbours(grid, position):
	x,y = position
	neighbour_cells = [(x - 1, y - 1), (x - 1, y + 0), (x - 1, y + 1),
					   (x + 0, y - 1),				 (x + 0, y + 1),
					   (x + 1, y - 1), (x + 1, y + 0), (x + 1, y + 1)]
	count = 0
	for x,y in neighbour_cells:
		if x >= 0 and y >= 0:
			try:
				count += grid[x][y]
			except:
				pass
	return count

def make_grid(x, y):
	grid = []
	if settings["isRandom"] == True:
		for i in range(x):
			row = []
			for j in range(y):
				row.append(random.randint(0, 1))
			grid.append(row)
	else:
		for i in range(x):
			row = []
			for j in range(y):
				if [i, j] in settings["map"]:
					row.append(1)
				else:
					row.append(0)
			grid.append(row)
	return grid

def make_empty_grid(x, y):
	grid = []
	for i in range(x):
		row = []
		for j in range(y):
			row.append(0)
		grid.append(row)
	return grid

def evolve_cell(alive, neighbours):
	willbe = 0
	if alive == 0:
		if neighbours in settings["rules"]["born"]:
			willbe = 1
	elif alive == 1:
		if neighbours in settings["rules"]["survive"]:
			willbe = 1
	return willbe

def evolve(grid):
	x = len(grid)
	y = len(grid[0])
	new_grid = make_empty_grid(x, y)
	for r in range(x):
		for c in range(y):
			cell = grid[r][c]
			neighbours = count_neighbours(grid, (r, c))
			new_grid[r][c] = 1 if evolve_cell(cell, neighbours) else 0
	return new_grid

screen = pg.display.set_mode([settings["sizeX"]*settings["cellSize"], settings["sizeY"]*settings["cellSize"]+50])
fontRenderer = pg.font.Font(None, 23)
pg.display.set_caption('Game of Life Simulation by ignitedoreo/cosmo')

grid = make_grid(settings["sizeX"], settings["sizeY"])

pg.display.flip()

running = True
while running:
	for event in pg.event.get():
		if event.type == pg.QUIT:
			running = False
		elif event.type == pg.MOUSEBUTTONDOWN:
			x, y = event.pos
			if y < settings["sizeX"]*settings["cellSize"]:
				x, y = x // settings["cellSize"], y // settings["cellSize"]
				if grid[x][y] == 1:
					grid[x][y] = 0
				elif grid[x][y] == 0:
					grid[x][y] = 1
			else:
				if x > 5 and x < 50 and y > settings["sizeY"]*settings["cellSize"]+5 and y < settings["sizeY"]*settings["cellSize"]+20:
					grid = make_empty_grid(settings["sizeX"], settings["sizeY"])
				if x > 5 and x < 145 and y > settings["sizeY"]*settings["cellSize"]+22 and y < settings["sizeY"]*settings["cellSize"]+40:
					if platform.system() == 'Darwin':
						subprocess.call(('open', os.path.abspath("settings.json")))
					elif platform.system() == 'Windows':
						os.startfile(os.path.abspath("settings.json"))
					else:
						subprocess.call(('xdg-open', os.path.abspath("settings.json")))
				if x > 150 and x < 290 and y > settings["sizeY"]*settings["cellSize"]+22 and y < settings["sizeY"]*settings["cellSize"]+40:
					pause_w = pause
					pause = True
					with open("map.txt", "w") as tfile:
						c, r = 0, 0
						file = "["
						for i in grid:
							for j in i:
								if j == 1:
									file = file + "[" + str(c) + ", " + str(r) + "], "
								r += 1
							c += 1
							r = 0
						file = file[:-2]
						file = file + "]\nPaste it to \"map\" in settings.json"
						tfile.write(file)
					pause = pause_w
		elif event.type == pg.KEYDOWN:
			if event.key == pg.K_SPACE:
				pause = not pause
	c, r = 0, 0
	for i in grid:
		for j in i:
			color = (settings["col1"][0], settings["col1"][1], settings["col1"][2]) if j == 1 else (settings["col3"][0], settings["col3"][1], settings["col3"][2])
			pixel(screen, color, c, r)
			if settings["grid"] == True:
				out(screen, (settings["col2"][0], settings["col2"][1], settings["col2"][2]), c, r)
			r += 1
		c += 1
		r = 0
	pg.draw.rect(screen, (settings["col3"][0], settings["col3"][1], settings["col3"][2]), (0, settings["sizeY"]*settings["cellSize"]+1, settings["sizeX"]*settings["cellSize"], 50))
	pg.draw.line(screen, (settings["col2"][0], settings["col2"][1], settings["col2"][2]), (0, settings["sizeY"]*settings["cellSize"]), (settings["sizeX"]*settings["cellSize"], settings["sizeY"]*settings["cellSize"]))
	pg.draw.rect(screen, (settings["col1"][0], settings["col1"][1], settings["col1"][2]), (5, settings["sizeY"]*settings["cellSize"]+5, 45, 15), width=1)
	reset = fontRenderer.render("Reset", True, (settings["col1"][0], settings["col1"][1], settings["col1"][2]))
	screen.blit(reset, (6, settings["sizeY"]*settings["cellSize"]+6))
	pg.draw.rect(screen, (settings["col1"][0], settings["col1"][1], settings["col1"][2]), (5, settings["sizeY"]*settings["cellSize"]+22, 140, 18), width=1)
	settf = fontRenderer.render("Open Settings File", True, (settings["col1"][0], settings["col1"][1], settings["col1"][2]))
	screen.blit(settf, (6, settings["sizeY"]*settings["cellSize"]+23))
	pg.draw.rect(screen, (settings["col1"][0], settings["col1"][1], settings["col1"][2]), (150, settings["sizeY"]*settings["cellSize"]+22, 140, 18), width=1)
	settf = fontRenderer.render("Export map to file", True, (settings["col1"][0], settings["col1"][1], settings["col1"][2]))
	screen.blit(settf, (151, settings["sizeY"]*settings["cellSize"]+23))
	settp = fontRenderer.render("Space to pause/unpause simulation.", True, (settings["col1"][0], settings["col1"][1], settings["col1"][2]))
	screen.blit(settp, (55, settings["sizeY"]*settings["cellSize"]+5))
	if pause == False:
		grid = evolve(grid)
	if pause == True:
		pg.draw.rect(screen, (settings["col2"][0], settings["col2"][1], settings["col2"][2]), (settings["sizeX"]*settings["cellSize"]-40, settings["sizeY"]*settings["cellSize"]+10, 10, 30))
		pg.draw.rect(screen, (settings["col2"][0], settings["col2"][1], settings["col2"][2]), (settings["sizeX"]*settings["cellSize"]-20, settings["sizeY"]*settings["cellSize"]+10, 10, 30))
	pg.display.flip()
	pg.time.wait(settings['sleepMiliseconds'])

pg.quit()
